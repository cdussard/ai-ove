from flask import Flask,render_template,url_for,request
import pandas as pd
import spacy
from spacy import displacy
nlp = spacy.load("fr_core_news_sm")

app = Flask(__name__)

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/process',methods=["POST"])
def process():
	if request.method == 'POST':
		choice = request.form['taskoption']
		rawtext = request.form['rawtext']
		doc = nlp(rawtext)
		d = []
		for ent in doc.ents:
			d.append((ent.label_, ent.text))
			df = pd.DataFrame(d, columns=('named entity', 'output'))
			ORG_named_entity = df.loc[df['named entity'] == 'ORG']['output']
			PERSON_named_entity = df.loc[df['named entity'] == 'PER']['output'] 
			LOC_named_entity = df.loc[df['named entity'] == 'LOC']['output'] 
			MISC_named_entity = df.loc[df['named entity'] == 'MONEY']['output']
		if choice == 'organisation':
			results = ORG_named_entity
			num_of_results = len(results)
		elif choice == 'person':
			results = PERSON_named_entity
			num_of_results = len(results)
		elif choice == 'localisation':
			results = LOC_named_entity
			num_of_results = len(results)
		elif choice == 'misc':
			results = MISC_named_entity
			num_of_results = len(results)
		
	
	return render_template("index.html",results=results,num_of_results = num_of_results)


if __name__ == '__main__':
	app.run(debug=True)